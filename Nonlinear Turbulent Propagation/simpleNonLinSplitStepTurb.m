function [XCut,TauCut,Energy,Z] =    ...
                           simpleNonLinSplitStepTurb(UIn,N0,Chi3,K0,K2, ...
                                                     Dxy,Dtau,Deta,L,   ...
                                                     NScreens,NLIter,   ...
                                                     Cn2,Ell0,L0)
%[XCut,TauCut,Z]=simpleNonLinSplitStepTurb(UIn,N0,Chi3,K0,K2,Dxy,Dtau,  ...
% Deta,L,NScreens,NLIter,Cn2,Ell0,L0)
% A simple symmetric non-linear Fourier split-step beam propagation in
% turbulence routine. Will return pulse's x-axis and tau-axis field
% profiles state and total energy at L and half-way between each phase
% screen.
% Inputs
%   UIn - initial pulse state, 3D array, [x,y,tau], V/m
%   N0 - medium's refractive index, scalar
%   Chi3 - medium's third-order nonlinear susceptibility, scalar, m^2/V^2
%   K0 - pulse's carrier wavenumber, scalar, rad/m
%   K2 - pulse's GVD parameter, scalar, s^2/m
%   Dxy - x/y grid spacing, scalar, m
%   Dtau - tau grid spacing, scalar, s
%   Deta - propagation step size, scalar, m
%   L - total propagation distance, scalar, m
%   NScreens - number of turbulent phase screens to apply, scalar
%   NLIter - number of iterations for nonlinear step evaluation, scalar
%   Cn2 - turbulence's refractive index structure constant, scalar, m^-2/3
%   Ell0 - turbulence's inner scale length, scalar, m
%   L0 - turbulence's outer scale length, scalar, m
% Outputs
%   XCut - Field profiles along x-axis, 2D array, [z,x], V/m
%   TauCut - Field profiles along tau-axis, 2D array, [z,tau], V/m
%   Energy - Pulse energy, 1D array, J
%   Z - propagation distances for UOut, 1D array, m  
% 
% Henry Elder, helder@umd.edu

% Speed of light
C    = 299792458; % /ms
MU0  = pi*4E-7; % H/m
EPS0 = 1/(MU0*C^2); % F/m

% Check grid sizing on UIn
if (ndims(UIn) ~= 3) ||            ... % UIn is 3D?
   (size(UIn,1) ~= size(UIn,2)) || ... % Spatial part is square?
   (rem(size(UIn,1),2) ~= 0) ||    ... % Spatial part has even samples?
   (rem(size(UIn,3),2) ~= 0)           % Temporal part has even samples?
    error(['This function requires a square transverse grid and ' ...
           'an even number of grid points for UIn']);
end

% Check that screen-to-screen distance is small enough for Rytov approx.
if 1.23*Cn2*K0^(7/6)*(L/NScreens)^(11/6) > 0.2
    warning(['Turbulence does not obey RytovVariance << 1. ' ...
             'Results may not be accurate']);
end

% Get Z domain for characterizations and phase screens
Z = (1:NScreens)*L/NScreens; % characterization distances, m
ZScreen = [0 Z(1:end-1)] + 0.5*diff([0,Z]); % phase screen distances, m
IndexZ = round(Z/Deta); % split-step indices for characterization
IndexScreen = round(ZScreen/Deta); % split-step indices for phase screens
NDeta = round(L/Deta); % total number of steps
if (max(IndexZ*Deta - Z) > 10*eps(L)) ||            ...
   (max(IndexScreen*Deta - ZScreen) > 10*eps(L)) || ...
   (rem(L,Deta) > 10*eps(L))
    warning(['Propagation step Deta, total propagation distance L, and' ...
             ' number of phase screens NScreen do not allow for an '    ...
             'integer number of steps between screens and '             ...
             'characterizations. Results may not be accurate']);
end

% Randomize the seed for MATLAB's random number generator
rng('shuffle');

% Define some useful parameters
Nxy = size(UIn,1); % number of spatial grid points
Ntau = size(UIn,3); % number of temporal grid points
DFxy = 1 / (Nxy * Dxy); % frequency domain grid spacing, 1/m
DFtau = 1 / (Ntau * Dtau); % frequency domain grid spacing, Hz
Gamma  = 3i*K0*Chi3/(8*N0^2); % nonlinear prefactor, m/V^2
EnergyFactor = N0*EPS0*C/2 * Dxy^2 * Dtau;

% Define phase factor for Fresnel propagation over distance DEta/2
[Nx3D,Ny3D,Ntau3D] = meshgrid(-Nxy/2:Nxy/2-1, ...
                              -Nxy/2:Nxy/2-1, ...
                              -Ntau/2:Ntau/2-1);
Fresnel = exp((1i/(2*K0) * (2i*pi*DFxy)^2*(Nx3D.^2+Ny3D.^2)    ...
              -1i*K2/2   * (-2i*pi*DFtau)^2*Ntau3D.^2) * Deta/2);

% Define supergaussian taper
SuperGauss = exp(-(Nx3D.^2+Ny3D.^2).^8/(0.43*Nxy)^16 ...
                 -Ntau3D.^16/(0.43*Ntau)^16);

% Initialze flag for when to compute phase screens
ScreenFlag = true;

% Initialize outputs
XCut = zeros(NScreens,Nxy);
TauCut = zeros(NScreens,Ntau);
Energy = zeros(NScreens,1);
% Begin split-step procedure
% hwb = waitbar(0,'Propagating');
for j = 1:NDeta
    % Propagate DEta/2
    UMiddle = ift2p1(Fresnel .* ft2p1(UIn,Dxy,Dtau),DFxy,DFtau);
    % First nonlinear iteration
    UNonLin = exp(Gamma * abs(UMiddle).^2 * Deta) .* UMiddle;
    UOut = ift2p1(Fresnel .* ft2p1(UNonLin,Dxy,Dtau),DFxy,DFtau);
    % Iterate nonlinear solution
    for jj = 1:NLIter
        % Use trapezoidal rule to estimate nonlinear contribution
        UNonLin = exp(Gamma * (abs(UIn).^2 + abs(UOut).^2) * Deta/2)  ...
                        .* UMiddle;
        UOut = ift2p1(Fresnel .* ft2p1(UNonLin,Dxy,Dtau),DFxy,DFtau);
    end
    % Check if we need to apply a phase screen
    if any(j == IndexScreen)
        % Apply phase screen
        if ScreenFlag % Need to compute screens
            [Phase1,Phase2] = pwdPhaseScreenMvk(K0,Nxy,Dxy,L/NScreens,  ...
                                                Cn2,Ell0,L0);
            UOut = UOut .* exp(1i*Phase1);
            ScreenFlag = false;
        else % Do not need to compute new screens
            UOut = UOut .* exp(1i*Phase2);
            ScreenFlag = true;
        end
    end
    % Check if we need to store beam state
    if any(j == IndexZ)
        % Extract desired parameters
        [~,ii] = find(j == IndexZ);
        XCut(ii,:) = squeeze(UOut(:,Nxy/2+1,Ntau/2+1));
        TauCut(ii,:) = squeeze(UOut(Nxy/2+1,Nxy/2+1,:));
        Energy(ii) = EnergyFactor * sum(abs(UOut).^2,'all');
    end
    % Apply taper and set UIn for next step
    UIn = SuperGauss .* UOut;
%     waitbar(j/NDeta(end),hwb);
end
% close(hwb);

return % simpleNonLinSplitStepTurb.m