% testNonLinSplitStepTurb_SCRIPT.m
% Demonstrate the use of simpleNonLinSplitStepTurb.m by observing the
% evolution of a short, Gaussian pulse.
%
% Henry Elder, helder@umd.edu

clear; close all; clc;
% Speed of light
C    = 299792458; % /ms
MU0  = pi*4E-7; % H/m
EPS0 = 1/(MU0*C^2); % F/m
% Specify beam parameters
LAM0 = 800E-9; % wavelength, m
W0   = 0.5E-3; % spot size, m
F0   = inf; % phase curvature, m
T0   = 650E-15; % length, sec
C0   = 0; % chirp
P0 = 2.65E6; % peak power, GW
% Specify medium parameters
N0 = 1.3296; % refractive index
K0 = 2*pi*N0/LAM0; % wavenumber, rad/m
K2 = 250E-28; % group velocity dispersion, s^2/m
CN2 = 0; % turbulence structure constant, m^-2/3
ELL0 = 0; % turbulence inner scale length, m
L0   = 0; % turbulence outer scale length, m
CHI3 = 2.5E-22; % third order nonlinear susceptibility, m^2/V^2
L    = 5; % total propagation distance, m
% Specify sampling parameters
NXY  = 256; % number of transverse grid points
DXY  = 14*W0/NXY; % transverse grid spacing, m
NTAU = 256; % number of temporal grid points
DTAU = 16*T0/NTAU; % temporal grid spacing, sec
NSCREEN = 10; % number of phase screens
DETA = 0.05; % step size, m
NMONTE = 1; % number of Monte Carlo iterations
NLITER = 2; % number of nonlinear iterations

% Now define the rest of the input parameters
X1D = (-NXY/2:NXY/2-1)*DXY;
Tau1D = (-NTAU/2:NTAU/2-1)*DTAU;
[X3D,Y3D,Tau3D] = meshgrid(X1D,X1D,Tau1D); % 3D X, Y, and Tau domains
U0  = (2/W0)*sqrt(P0/(pi*N0*EPS0*C)); % peak field, V/m
UIn = U0 * exp(-(1/W0^2+0.5i*K0/F0)*(X3D.^2+Y3D.^2) ...
               -(1+1i*C0)*Tau3D.^2/T0^2); % input Gaussian pulse, V/m

% Run simulations
[XCut,TauCut,Energy,Z] =      ...
    simpleNonLinSplitStepTurb(UIn,N0,CHI3,K0,K2, ...
                              DXY,DTAU,DETA,L,   ...
                              NSCREEN,NLITER,    ...
                              CN2,ELL0,L0);

% Compute linear, non-turbulence evolution for comparison
[Wz,Tz,Uz] = linearGaussianEvolution(K0,W0,F0,T0,C0,K2,Z);

%% Display results
tiledlayout(1,2);
% Plot x cuts
nexttile;
plot3(zeros(1,NXY),X1D*1E3,abs(squeeze(UIn(:,NXY/2+1,NTAU/2+1)))*1E-6, ...
      'LineWidth',2);
hold on;
for j = 1:NSCREEN
    plot3(Z(j)*ones(1,NXY),X1D*1E3,abs(XCut(j,:))*1E-6, ...
          'LineWidth',2);
    plot3(Z(j)*ones(1,NXY),X1D*1E3,U0*Uz(j)*exp(-X1D.^2/Wz(j).^2)*1E-6, ...
          'k--','LineWidth',2);
end
grid on; grid minor;
xlabel('Propagation Distance $z$, meters','interpreter','latex');
ylabel('Transverse Coordinate $x$, mm','interpreter','latex');
zlabel('Field Magnitude, MV/m','interpreter','latex');
title('$|U(x,y=0,\tau=0)|$ Evolution','interpreter','latex');
% Plot tau cuts
nexttile;
plot3(zeros(1,NTAU),Tau1D*1E12,                 ...
      abs(squeeze(UIn(NXY/2+1,NXY/2+1,:)))*1E-6,...
      'LineWidth',2);
hold on;
for j = 1:NSCREEN
    plot3(Z(j)*ones(1,NTAU),Tau1D*1E12,abs(TauCut(j,:))*1E-6, ...
          'LineWidth',2);
    plot3(Z(j)*ones(1,NTAU),Tau1D*1E12,             ...
          U0*Uz(j)*exp(-Tau1D.^2/Tz(j).^2)*1E-6,    ...
          'k--','LineWidth',2);
end
grid on; grid minor;
xlabel('Propagation Distance $z$, meters','interpreter','latex');
ylabel('Retarded Time Coordinate $\tau$, ps','interpreter','latex');
zlabel('Field Magnitude, MV/m','interpreter','latex');
title('$|U(x=0,y=0,\tau)|$ Evolution','interpreter','latex');
% End of testNonLinSplitStepTurb_SCRIPT.m