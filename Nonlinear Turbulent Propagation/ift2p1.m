function g = ift2p1(f,dfxy,dft)
%G=IFT2P1(F,DFXY,DFT)
% 3D (2D space, 1D time) inverse Fourier transform -- consolidated
% indexing of f should be [space,space,time]
g = ifftshift(ifftn(ifftshift(f))) * (size(f,1)*dfxy)^2 * size(f,3)*dft;
end % ift2p1.m