function f = ft2p1(g,dxy,dt)
%F=FT2P1(G,DXY,DT)
% 3D (2D space, 1D time) inverse Fourier transform -- consolidated
f = fftshift(fftn(fftshift(g))) * dxy^2 * dt;
end % ft2p1.m