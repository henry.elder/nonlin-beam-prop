function [A1Out,A2Out,Z] =  ...
            simpleNonLinSplitStepFiber(A1In,Omega1,Betas1,Qspm1, ...
                                       A2In,Omega2,Betas2,Qspm2, ...
                                       Beta1Ref,Qxpm,Chi3,       ...
                                       Dtau,Deta,L,NSaves,NLIter)
%[A1Out,A2Out,Z]= ...
%   simpleNonLinSplitStepFiber(A1In,Omega1,Betas1,Qspm1, ...
%                              A2In,Omega2,Betas2,Qspm2, ...
%                              Beta1Ref,Qxpm,Chi3,       ...
%                              Dtau,Deta,L,NSaves,NLIter)
% A simple symmetric non-linear Fourier split-step beam propagation in
% fiber routine for two pulses. Will return the pulses' states at L and
% NSaves distances along the fiber.
% Inputs
%   A1In - initial pulse state for mode 1, 1D array, [tau], W
%   Omega1 - mode 1 carrier frequency, scalar, rad/s
%   Betas1 - dispersion parameters for mode 1 as a Taylor series, 1D array,
%       SI units
%   Qspm1 - self-phase modulation overlap integral for mode 1, scalar, 
%       (V/m)^4/(W/m)^2
%   A2In, Omega1, Betas2, Qspm2 - same as above, but for mode 2
%   Beta1Ref - reference inverse-group-velocity for retarded time frame,
%       scalar, s/m
%   Qxpm - cross-phase modulation overlap integral, scalar, (V/m)^4/(W/m)^2
%   Chi3 - third-order nonlinear susceptibility of fiber, scalar, m^2/V^2
%   Dtau - tau grid spacing, scalar, s
%   Deta - propagation step size, scalar, m
%   L - total propagation distance, scalar, m
%   NSaves - number of locations along fiber to save pulse states, scalar
%   NLIter - number of iterations for nonlinear step evaluation, scalar
% Outputs
%   A1Out - output mode 1 state, 2D array, [step,tau], W
%   A2Out - output mode 2 state, 2D array, [step,tau], W
%   Z - propagation distances for A1Out and A2Out, 1D array, m  
% 
% Henry Elder, helder@umd.edu

% Speed of light
C    = 299792458;
MU0  = pi*4E-7;
EPS0 = 1/(MU0*C^2);

% Check grid sizing on A1In and A2In
if (length(A1In) ~= length(A2In)) || ... % A1In and A2In same length?
   (rem(length(A1In),2) ~= 0)            % Even number of samples?
    error(['This function requires the input mode profiles to have the '...
           'same length, and have an even number of grid points']);
end

% Get Z domain for state characterizations
Z = (1:NSaves)*L/NSaves; % characterization distances, m
IndexZ = round(Z/Deta); % split-step indices for characterization
NDeta = round(L/Deta); % total number of steps
if (max(IndexZ*Deta - Z) > 10*eps(L)) ||            ...
   (rem(L,Deta) > 10*eps(L))
    warning(['Propagation step Deta, total propagation distance L, and' ...
             ' number of characterizations NSaves do not allow for an ' ...
             'integer number of steps between characterizations. '      ...
             'Results may not be accurate']);
end

% Define some useful parameters
Ntau = length(A1In); % number of temporal grid points
DFtau = 1 / (Ntau * Dtau); % frequency domain grid spacing, Hz
Alpha1 = 1i*EPS0*Chi3*Omega1/8; % (complex) mode 1 NL prefactor
Alpha2 = 1i*EPS0*Chi3*Omega2/8; % (complex) mode 2 NL prefactor

% Define phase factors for linear propagation over distance DEta/2
Ntau1D = (-Ntau/2:Ntau/2-1);
% Accrue dispersion exponent for mode 1
DispersionPhase1 = (Beta1Ref - Betas1(1)) * (-2i*pi*DFtau*Ntau1D);
for j = 2:length(Betas1)
    DispersionPhase1 = DispersionPhase1 ...
          + (1i)^(j+1)/factorial(j) * Betas1(j) * (-2i*pi*DFtau*Ntau1D).^j;
end
% Convert to phase factor over Deta/2
Dispersion1 = exp(DispersionPhase1 * Deta/2);
% Accrue dispersion exponent for mode 2
DispersionPhase2 = (Beta1Ref - Betas2(1)) * (-2i*pi*DFtau*Ntau1D);
for j = 2:length(Betas2)
    DispersionPhase2 = DispersionPhase2 ...
          + (1i)^(j+1)/factorial(j) * Betas2(j) * (-2i*pi*DFtau*Ntau1D).^j;
end
% Convert to phase factor over Deta/2
Dispersion2 = exp(DispersionPhase2 * Deta/2);

% Define supergaussian taper
SuperGauss = exp(-Ntau1D.^16/(0.43*Ntau)^16);

% Begin split-step procedure
A1Out = single(zeros(NSaves,Ntau));
A2Out = single(zeros(NSaves,Ntau));
hwb = waitbar(0,'Propagating');
for j = 1:IndexZ(end)
    % Propagate DEta/2
    A1Middle = ift1(Dispersion1 .* ft1(A1In,Dtau),DFtau);
    A2Middle = ift1(Dispersion2 .* ft1(A2In,Dtau),DFtau);
    % First nonlinear iteration
    A1NonLin = exp(Alpha1*Deta *    ...
                    (Qspm1*abs(A1Middle).^2+Qxpm*abs(A2Middle).^2)) ...
                .* A1Middle;
    A2NonLin = exp(Alpha2*Deta *    ...
                    (Qxpm*abs(A1Middle).^2+Qspm2*abs(A2Middle).^2)) ...
                .* A2Middle;
    A1End = ift1(Dispersion1 .* ft1(A1NonLin,Dtau),DFtau);
    A2End = ift1(Dispersion2 .* ft1(A2NonLin,Dtau),DFtau);
    % Iterate nonlinear solution
    for jj = 1:NLIter
        % Use trapezoidal rule to estimate nonlinear contribution
        A1NonLin = exp(Alpha1*Deta/2 *                  ...
                    (Qspm1*(abs(A1In).^2+abs(A1End).^2) ...
                    +Qxpm*(abs(A2In).^2+abs(A2End).^2))) .* A1Middle;
        A2NonLin = exp(Alpha2*Deta/2 *                  ...
                    (Qxpm*(abs(A1In).^2+abs(A1End).^2) ...
                    +Qspm2*(abs(A2In).^2+abs(A2End).^2))) .* A2Middle;
        A1End = ift1(Dispersion1 .* ft1(A1NonLin,Dtau),DFtau);
        A2End = ift1(Dispersion2 .* ft1(A2NonLin,Dtau),DFtau);
    end
    % Check if we need to store beam state
    if any(j == IndexZ)
        % Save envelopes
        [~,ii] = find(j == IndexZ);
        A1Out(ii,:) = A1End;
        A2Out(ii,:) = A2End;
    end
    % Apply taper and set AIn for next step
    A1In = SuperGauss .* A1End;
    A2In = SuperGauss .* A2End;
    waitbar(j/NDeta(end),hwb);
end
close(hwb);

return % simpleNonLinSplitStepFiber.m