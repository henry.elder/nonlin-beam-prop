function f = ft1(g,dx)
%F=FT1(G,DX)
%1D Fourier transform -- consolidated
f = fftshift(fft2(fftshift(g))) * dx;
end % ft1.m