function g = ift1(f,df)
%G=IFT1(F,DF)
%1D inverse Fourier transform -- consolidated
g = ifftshift(ifft2(ifftshift(f))) * (length(f)*df);
end % ift1.m