% testNonLinSplitStepFiber_SCRIPT.m
% Demonstrate the use of simpleNonLinSplitStepFiber.m using the
% "bright-dark" soliton pair example from Agrawal, "Nonlinear Fiber Optics,
% 5ed", Academic Press (2013), section 7.3.1. Mode 1 will be the "dark"
% soliton -- that is, it the "turning off" of a CW beam that acts as the
% wave. Mode 2 is the "bright" soliton, which is just the opposite (and the
% usual case). This test case demonstrates the accurate implementation if
% dispersion (to second order), SPM, and XPM.
%
% Henry Elder, helder@umd.edu

clear; close all; clc;
% Speed of light
C    = 299792458; % m/s
MU0  = pi*4E-7; % H/m
EPS0 = 1/(MU0*C^2); % F/m
% Specify some beam and fiber parameters 
LAM1 = 1450E-9; % mode 1 wavelength, m
BETAS1 = [1.5/C, -1.5E-25]; % mode 1 dispersion, s/m and s^2/m
QSPM1 = 1E15; % mode 1 SPM overlap integral, V^4/W^2-m^2
BETAS2 = [BETAS1(1),  1.0E-25]; % mode 2 dispersion, s/m and s^2/m
QSPM2 = QSPM1; % mode 2 SPM overlap integral, V^4/W^2-m^2 
LAMREF = 1100E-9; % reference (central) wavelength, m
QXPM = 2*QSPM1; % mode 1-2 XPM overlap integral, V^4/W^2-m^2
T0   = 100E-15; % length of solitons, sec
CHI3 = 2.5E-22; % third order nonlinear susceptibility, m^2/V^2
L     = 0.5; % total propagation distance, m
% Specify sampling parameters
NTAU  = 2^19; % number of temporal grid points
DTAU  = 400*T0/NTAU; % temporal grid spacing, sec
NSAVE = 5; % number of intermediate save points
DETA  = 1E-3; % step size, m
NLITR = 2; % number of nonlinear iterations

% Now define the rest of the input parameters
FreqRef = 2*pi*C/LAMREF; % central frequency, rad/sec
Omega1  = 2*pi*C/LAM1 - FreqRef; % mode 1 detuning, rad/sec
Omega2  = -abs(BETAS1(2))*Omega1/BETAS2(2); % mode 2 detuning, rad/sec
Gamma1 = EPS0*CHI3*(FreqRef+Omega1)*QSPM1/8; % mode 1 nonlinear parameter
Gamma2 = EPS0*CHI3*(FreqRef+Omega2)*QSPM2/8; % mode 2 nonlinear parameter
B1sqrd = (2*Gamma1*BETAS2(2)-Gamma2*BETAS1(2))/(3*T0^2*Gamma1*Gamma2);
B2sqrd = (Gamma1*BETAS2(2)-2*Gamma2*BETAS1(2))/(3*T0^2*Gamma1*Gamma2);
Beta1Ref = BETAS1(1) - BETAS1(2)*Omega1; % reference group velocity, s/m

% Define input soliton waveforms
% Taper the bright soliton to avoid wraparound effects
NTau1D = -NTAU/2:NTAU/2-1;
Tau = NTau1D*DTAU; % Retarded time domain, sec
SuperGauss = exp(-NTau1D.^16/(0.43*NTAU)^16);
A1In = sqrt(B1sqrd)*tanh(Tau/T0).*exp(-1i*Omega1*Tau) .* SuperGauss;
A2In = sqrt(B2sqrd)*sech(Tau/T0).*exp(-1i*Omega2*Tau);

% Run simulation
[A1Out,A2Out,Z] =  ...
         simpleNonLinSplitStepFiber(A1In,(FreqRef+Omega1),BETAS1,QSPM1, ...
                                    A2In,(FreqRef+Omega2),BETAS2,QSPM2, ...
                                    Beta1Ref, QXPM, CHI3,               ...
                                    DTAU,DETA,L,NSAVE,NLITR);

% Display results
tiledlayout(1,2); 
% Plot dark soliton (mode 1)
nexttile;
plot3(zeros(1,NTAU),Tau*1E15,abs(A1In),'linewidth',2);
hold on;
for j = 1:NSAVE
    plot3(Z(j)*ones(1,NTAU),Tau*1E15,abs(A1Out(j,:)),'linewidth',2);
end
grid; grid minor; ylim([-5*T0 5*T0]*1E15);
xlabel('Propagation Distance $z$, meters','interpreter','latex');
ylabel('Retarded Time $\tau$, fs','interpreter','latex');
zlabel('Field Magnitude, V/m','interpreter','latex');
title('Dark Soliton Evolution','interpreter','latex');
% Plot bright soliton (mode 2)
nexttile;
plot3(zeros(1,NTAU),Tau*1E15,abs(A2In),'linewidth',2); hold on;
for j = 1:NSAVE
    plot3(Z(j)*ones(1,NTAU),Tau*1E15,abs(A2Out(j,:)),'linewidth',2);
end
grid; grid minor; ylim([-5*T0 5*T0]*1E15);
xlabel('Propagation Distance, $z$, m','interpreter','latex');
ylabel('Retarded Time $\tau$, fs','interpreter','latex');
zlabel('Field Magnitude, V/m','interpreter','latex');
title('Bright Soliton Evolution','interpreter','latex');

% End of testNonLinSplitStepFiber_SCRIPT.m