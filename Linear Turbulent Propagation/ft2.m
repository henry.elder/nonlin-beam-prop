function f = ft2(g,dxy)
%f=ft2(g,dxy)
% 2D Fourier transform -- consolidated
f = fftshift(fftn(fftshift(g))) * dxy^2;
end % ft2.m