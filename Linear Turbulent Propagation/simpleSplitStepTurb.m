function [UOut,Z] = simpleSplitStepTurb(UIn,K0,Dxy,L,NScreen,Cn2,Ell0,L0)
%[UOut,Z]=simpleSplitStepTurb(UIn,K0,Dxy,L,NScreen,Cn2,Ell0,L0)
% A simple Fourier split-step beam propagation in turbulence routine. Will
% return the pulse states at L and half-way between each phase screen.
% Output will be single precision to aid in memory management.
% Inputs
%   UIn - initial beam state, 2D array, [x,y], V/m
%   K0 - carrier wavenumber, scalar, rad/m
%   Dxy - X/Y grid spacing for uin, scalar, m
%   L - total propagation distance, scalar, m
%   NScreen - number of phase screens, scalar
%   Cn2 - turbulence's refractive index structure constant, scalar, m^-2/3
%   Ell0 - turbulence's inner scale length, scalar, m
%   L0 - turbulence's outer scale length, scalar, m
% Outputs
%   UOut - output beam state, 3D array, [step,x,y], V/m
%   Z - propagation distances for UOut, 1D array, m
%
% Henry Elder, helder@umd.edu

% Check grid sizing on UIn
if ~ismatrix(UIn) ||               ... % UIn is 2D?
   (size(UIn,1) ~= size(UIn,2)) || ... % UIn is square?
   (rem(size(UIn,1),2) ~= 0)        % UIn has an even number of samples?
    error(['This function requires a square grid with ' ...
           'an even number of grid points for UIn']);
end

% Check that DZ is small enough for Rytov approximation
if 1.23*Cn2*K0^(7/6)*(L/NScreen)^(11/6) > 0.2
    warning(['Turbulence does not obey RytovVariance << 1. ' ...
             'Results may not be accurate']);
end

% Randomize the seed for MATLAB's random number generator
rng('shuffle');

% Define some useful parameters
Nxy = size(UIn,1); % number of grid points
DFxy = 1 / (Nxy * Dxy); % frequency domain grid spacing, 1/m
Dz = L / NScreen; % distance for each screen's contribution, m
Z = (1:NScreen)*L/NScreen; % propagation distances for UOut, m

% Define phase factor for Fresnel propagation over distance Dz/2
[Nx2D,Ny2D] = meshgrid(-Nxy/2:Nxy/2-1);
Fresnel = exp(1i/(2*K0) * (2i*pi*DFxy)^2*(Nx2D.^2+Ny2D.^2) * Dz/2);

% Define supergaussian taper (absorbing boundary conditions)
SuperGauss = exp(-(Nx2D.^2+Ny2D.^2).^8/(0.43*Nxy)^16);

% Initialze flag for when to compute phase screens
ScreenFlag = true;

% Begin split-step procedure
UOut = single(zeros(NScreen,Nxy,Nxy));
for j = 1:NScreen
    % Propagate dz/2
    UMiddle = ift2(Fresnel .* ft2(UIn,Dxy),DFxy);
    % Apply phase screen
    if ScreenFlag % Need to compute screens
        [Phase1,Phase2] = pwdPhaseScreenMvk(K0,Nxy,Dxy,Dz,Cn2,Ell0,L0);
        UMiddle = UMiddle .* exp(1i*Phase1);
        ScreenFlag = false;
    else % Do not need to compute new screens
        UMiddle = UMiddle .* exp(1i*Phase2);
        ScreenFlag = true;
    end
    % Propagate another dz/2, and reset for next step
    UIn = SuperGauss .* ift2(Fresnel .* ft2(UMiddle,Dxy),DFxy);
    % Save beam state
    UOut(j,:,:) = single(UIn);
end

return % simpleSplitStep.m