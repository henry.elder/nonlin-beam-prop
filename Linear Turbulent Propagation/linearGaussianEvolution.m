function [Wz,Tz,Uz] = linearGaussianEvolution(K0,W0,F0,T0,C0,K2,Z)
%[Wz,Tz,Uz]=linearGaussianEvolution(K0,W0,F0,T0,C0,K2,Z)
% Calculate the spot-size, duration, and amplitude evolution of a Gaussian
% pulse in free space.
% Inputs
%   K0 - wavenumber, scalar, rad/m
%   W0 - initial spot size, scalar, m
%   F0 - initial phase curvature, scalar, m
%   T0 - initial pulse length, scalar, sec
%   C0 - initial pulse chirp
%   K2 - group velocity dispersion, scalar, m^2/sec
%   Z  - propagation distance(s), 1D array, m
% Outputs
%   Wz - spot size at Z distance(s), 1D array, m
%   Tz - pulse length at Z distance(s), 1D array, sec
%   Uz - (fractional) peak amplitude at Z distance(s), 1D array
%
% Henry Elder, helder@umd.edu

% Spot size evolution
Wz = W0 * sqrt((1-Z/F0).^2 + (2*Z/(K0*W0^2)).^2);

% Pulse length evolution
Tz = T0 * sqrt((1+2*C0*K2*Z/T0^2).^2 + (2*K2*Z/T0^2).^2);

% Amplitudes via conservation of energy
Uz =  (W0./Wz) .* sqrt(T0./Tz);

return % LinearGaussianEvolution.m