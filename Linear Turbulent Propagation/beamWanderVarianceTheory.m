function RcSquared = beamWanderVarianceTheory(K0,W0,F0,Cn2,Z)
%RcSquared = beamWanderVarianceTheory(K0,W0,F0,CN2,Z)
% Calculate theoretical beam wander variance for a Gaussian in Kolmogorov
% turbulence, based on Klyatskin, V., Kon, I., On the displacement of
% spatially-bounded light beams in a turbulent medium in the
% Markovian-random-process approximation, Radiophys. Quantum Electron., 15,
% 1051-1061 (1972).
% Inputs
%   K0 - wavenumber, scalar, rad/m
%   W0 - initial spot size, scalar, m
%   F0 - initial phase curvature, scalar, m
%   Cn2 - turbulence structure constant, scalar, m^-2/3
%   Z - propagation distance(s), 1D array, m
RcSquared = zeros(1,length(Z));
for j = 1:length(Z)
    Integrand = @(K,z) (Z(j)-z).^2 .* K.^3 .* (0.033*Cn2*K.^(-11/3))    ...
        .* exp(-(K.^2*W0^2/4) .* ((1-z/F0).^2 + (2*z/(K0*W0^2)).^2)      ...
        - 2*pi * (K.*z/K0).^(5/3) .* (0.55*K0^2*z*Cn2));
    RcSquared(j) = 4*pi^2*integral2(Integrand, 0, inf, 0, Z(j));
end

end % beamWanderVarianceTheory.m