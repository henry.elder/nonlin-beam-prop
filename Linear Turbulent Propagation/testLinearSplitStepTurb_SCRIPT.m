% testLinearSplitStepTurb_SCRIPT.m
% Demonstrate the use of simpleSplitStepTurb.m by comparing the on-axis
% scintillation and beam centroid wander of a Gaussian beam in Kolmogorov
% turbulence against theoretical results. This test case demonstrates the
% accurate implementation of diffraction and turbulence.
%
% Henry Elder, helder@umd.edu

clear; close all; clc;
% Specify beam parameters
LAM0 = 1600E-9; % wavelength, m
W0 = 0.1; % initial spot size, m
F0 = inf; % initial phase curvature, m
% Specify the air channel parameters
N0 = 1.0003; % refractive index
K0 = 2*pi*N0/LAM0; % wavenumber, rad/m
CN2 = 1.39E-15; % structure constant, m^-2/3
ELL0 = 0; % inner scale length, m
L0 = inf; % outer scale length, m
L  = 10E3; % total propagation distance, m
% Specify sampling parameters
DXY = 7.5E-3; % grid spacing, m
NXY = 256; % number of grid points
NSCREEN = 10; % number of phase screens
NMONTE = 1000; % number of Monte Carlo iterations

% Define input (Gaussian) waveform
X1D = (-NXY/2:NXY/2-1)*DXY;
[X2D,Y2D] = meshgrid(X1D,X1D); % 2D X and Y domains, m
UIn = exp(-(1/W0^2+0.5i*K0/F0)*(X2D.^2+Y2D.^2)); % input Gaussian, V/m

% Run simulation
Intensity0 = zeros(NMONTE,NSCREEN); % on-axis intensity, V^2/m^2
Xc = zeros(NMONTE,NSCREEN); % X centroid positions, m
Yc = zeros(NMONTE,NSCREEN); % Y centroid positions, m
hw = waitbar(0,'Generating Monte Carlo Ensemble of Simulation Results');
for j = 1:NMONTE
    [UOut,Z] = simpleSplitStepTurb(UIn,K0,          ...
                                   DXY,L,NSCREEN,   ...
                                   CN2,ELL0,L0);
    % Extract and save on-axis intensity and beam centroid position for
    % this iteration
    for jj = 1:NSCREEN
        Intensity = abs(squeeze(UOut(jj,:,:))).^2;
        Power = sum(Intensity,'all') * DXY^2;
        Intensity0(j,jj) = Intensity(NXY/2+1,NXY/2+1);
        Xc(j,jj) = sum(X2D.*Intensity,'all') * DXY^2 / Power;
        Yc(j,jj) = sum(Y2D.*Intensity,'all') * DXY^2 / Power;
    end
    waitbar(j/NMONTE,hw);
end
close(hw);

% Display results
figure;
tiledlayout(2,2);
% Plot input waveform
nexttile;
imagesc(X1D*1E2,X1D*1E2,abs(UIn));
xlabel('x Direction, cm','interpreter','latex');
ylabel('y Direction, cm','interpreter','latex');
title('Input Field Magnitude, V/m','interpreter','latex');
axis square;
% Plot output waveform for last simulation
nexttile;
imagesc(X1D*1E2,X1D*1E2,squeeze(abs(UOut(end,:,:))));
xlabel('x Direction, cm','interpreter','latex');
ylabel('y Direction, cm','interpreter','latex');
title('Output Field Magnitude (Single Simulation), V/m', ...
      'interpreter','latex');
axis square;
% Plot on-axis intensity
nexttile;
fplot(@(z) onAxisIntensityTheory(K0,W0,F0,CN2,ELL0,L0,z*1E3),[0,L*1E-3],...
      'LineWidth',2);
hold on;
plot(Z*1E-3,mean(Intensity0,1),'o','LineWidth',2,'MarkerSize',12);
grid; grid minor;
xlabel('Propagation Distance $z$, km','interpreter','latex');
ylabel('Average On-Axis Intensity $\langle I(0) \rangle$, V$^2$/m$^2$', ...
       'interpreter','latex');
title('Average On-Axis Intensity of a Gaussian in Turbulence', ...
      'interpreter','latex');
legend('Theory (Andrews and Phillips, 2005)','Simulation',...
       'interpreter','latex');
% Plot beam wander variance
nexttile;
fplot(@(z) 1E4*beamWanderVarianceTheory(K0,W0,F0,CN2,z*1E3),[0,L*1E-3], ...
      'LineWidth',2);
hold on;
plot(Z*1E-3,1E4*mean(Xc.^2+Yc.^2,1),'o','LineWidth',2,'MarkerSize',12);
grid; grid minor;
xlabel('Propagation Distance $z$, km','interpreter','latex');
ylabel('Beam Centroid Variance $\langle r_c^2 \rangle$, cm$^2$', ...
       'interpreter','latex');
title('Beam Wander Variance of a Gaussian in Turbulence', ...
      'interpreter','latex');
legend('Theory (Klyatskin \& Kon, 1972)','Simulation',...
       'interpreter','latex');
% End of testLinearSplitStepTurb_SCRIPT.m