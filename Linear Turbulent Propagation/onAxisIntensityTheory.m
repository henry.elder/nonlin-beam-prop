function Intensity0 = onAxisIntensityTheory(K0,W0,F0,Cn2,Ell0,L0,Z)
%Intensity0 = onAxisIntensityTheory(K0,W0,F0,Cn2,Ell0,L0,Z)
% Calcualte the theoretical value for ensemble average on-axis intensity of
% a Gaussian beam in modified von Karman turbulence. Based on the equations
% presented in Andrews, L., Phillip, R., "Laser Beam Propagation Through
% Random Media", 2ed, SPIE (2005).
% Inputs
%   K0 - wavenumber, scalar, rad/m
%   W0 - initial spot size, scalar, m
%   F0 - initial phase curvature, scalar, m
%   Cn2 - turbulence structure constant, scalar, m^-2/3
%   Ell0 - turbulence inner scale length, m
%   L0 - turbulence outer scale length, m
%   Z - propagation distance(s), 1D array, m
% Outputs
%   Intensity0 - ensemble average on-axis intensity at Z, 1D array, V^2/m^2
%
% Henry Elder, helder@umd.edu

% Calculate diffractive spot size, m
Wz = linearGaussianEvolution(K0,W0,F0,inf,0,0,Z);
% Calculate Fresnel term
Lambda = 2*Z./(K0*Wz.^2);
% Loop through propagation distances
Intensity0 = zeros(1,length(Z));
for j = 1:length(Z)
    % Define integrand (with spherical wave structure function)
    if and(Ell0==0,isinf(L0)) % Kolmogorov case
        Integrand = @(Q) Q .* exp(-K0*Q.^2./(4*Lambda(j)*Z(j))) ...
            .* exp(-0.545*Cn2*K0^2*Z(j)*Q.^(5/3));
    else % Modified von Karman turbulence case
        Integrand = @(Q) Q .* exp(-K0*Q.^2./(4*Lambda(j)*Z(j))) ...
            .* exp(-0.545*Cn2*K0^2*Z(j)*Ell0^(-1/3)*Q.^2 .*     ...
                    ((1+Q.^2/Ell0^2).^(-1/6) - 1.329*(Ell0/L0)^(1/3)));
    end
    % Evaluate integrand
    Intensity0(j) = K0^2*W0^2/(4*Z(j)^2) * integral(Integrand,0,10*W0);
end

return % onAxisIntensityTheory.m