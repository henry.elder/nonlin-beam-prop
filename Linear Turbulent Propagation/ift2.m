function g = ift2(f,dfxy)
%g=ift2(f,dfxy)
% 2D inverse Fourier transform -- consolidated
g = ifftshift(ifftn(ifftshift(f))) * (size(f,1)*dfxy)^2;
end % ift2.m