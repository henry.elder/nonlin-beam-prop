function [Phase1,Phase2] = pwdPhaseScreenMvk(K0,Nxy,Dxy,Dz,Cn2,Ell0,L0)
%[Phase1,Phase2] = pwdPhaseScreenMvk(K0,Nxy,Dxy,Dz,Cn2,Ell0,L0)
% Create two phase screens based on the Paulson-Davis-Wu method for a
% modified von Karman turbulent spectrum. See D. Paulson, C. Wu and C.
% Davis, "Randomized Spectral Sampling for Efficient Simulation of Laser
% Propagation Through Optical Turbulence," Journal of the Optical Society
% of America B, vol. 36, no. 11, pp. 3249-3262, 2019, and M. Charnotskii,
% Four code examples for turbulent phase screens simulation,
% https://doi.org/10.6084/m9.figshare.10565714.v2 (retrieved Sept. 2023)
% Inputs
%   K0 - carrier wavenumber, rad/m
%   Nxy - grid size
%   Dxy - grid spacing, m
%   Dz - propagating step distance, m
%   Cn2 - turbulence's refractive index structure constant, m^-2/3
%   Ell0 - turbulence's inner scale length, m
%   L0 - turbulence's outer scale length, m
% Outputs
%   Phase1 and Phase2: two independent phase screens
% 
% Henry Elder, helder@umd.edu

% Number of subharmonic layers
NSH = 5;

% Spatial domain, m
X = (-Nxy/2:Nxy/2-1)*Dxy; % row vector
Y = X'; % column vector

% Frequency domain, rad/m
Dk = 2*pi/(Nxy*Dxy);
K = (-Nxy/2:Nxy/2-1)*Dk;

% Subharmonics prep arguments
% Subharmonic frequency spacings
DkSubHarm = Dk ./ 3.^(1:NSH);
% Make matrices of each element, 3x3 each (3x3xNSH)
DkSubHarm3D = reshape(repmat(DkSubHarm,9,1),3,3,NSH);
% "Columnize"
DkSubHarm1D = reshape(DkSubHarm3D,9*NSH,1);
% Copy NSH times (3x3xNSH)
SubHarmGridX1D = repmat([-1 0 1],3,1);
SubHarmGridY1D = SubHarmGridX1D';
SubHarmGridX3D = repmat(SubHarmGridX1D,1,1,NSH);
SubHarmGridY3D = repmat(SubHarmGridY1D,1,1,NSH);

% Build PWD phase plane
% Randomly displaced DFT wavevectors (iid U(-Dk/2,Dk/2))
Kx_r = Dk * (rand - 0.5);
Ky_r = Dk * (rand - 0.5);
Kx = K  + Kx_r; % row vector
Ky = K' + Ky_r; % column vector
% High frequency IFT exponent
Expo = exp(-1i*Ky_r.*Y) * exp(-1i*Kx_r.*X); % (NXYxNXY)
% Compute phase PSD
KMatrix = sqrt(repmat(Kx,Nxy,1).^2 + repmat(Ky,1,Nxy).^2);
PowerSpectrum = 2*pi*0.033*K0^2*Cn2*Dz * exp(-(KMatrix/(5.92/Ell0)).^2) ...
                     ./ (KMatrix.^2+(2*pi/L0)^2).^(11/6);
% Remove DC value
PowerSpectrum(Nxy/2+1,Nxy/2+1) = 0;
% Random draw for coefficients
Coefs = (randn(Nxy) + 1i*randn(Nxy)) .* sqrt(PowerSpectrum) * Dk;
% FT for phase screen
% Note that real and imaginary parts are uncorrelated screens
ScreenHighFreq = ft2(Coefs, 1) .* Expo;

% Now build subharmonics
% Wavenumber coordinates for each subharmonic
KSubHarmX = DkSubHarm3D .* (SubHarmGridX3D + (rand(3,3,NSH)-0.5));
KSubHarmY = DkSubHarm3D .* (SubHarmGridY3D + (rand(3,3,NSH)-0.5));
% Columnize
KSubHarmX1D = reshape(KSubHarmX,9*NSH,1);
KSubHarmY1D = reshape(KSubHarmY,9*NSH,1)';
% Compute phase power spectral density
KMatrixSubHarm = sqrt(KSubHarmX1D.*KSubHarmX1D  ...
                      + (KSubHarmY1D.*KSubHarmY1D)');
PowerSpectrumSubHarm = 2*pi*0.033*K0^2*Cn2*Dz               ...
                    * exp(-(KMatrixSubHarm/(5.92/Ell0)).^2) ...
                    ./ (KMatrixSubHarm.^2+(2*pi/L0)^2).^(11/6);
% Remove DC value for all but highest subharmonic
PowerSpectrumSubHarm(5:9:9*(NSH-1)) = 0;
% Random draw for coefficients
Coefs = (randn(9*NSH,1) + 1i*randn(9*NSH,1))    ...
        .* sqrt(PowerSpectrumSubHarm) .* DkSubHarm1D;
ScreenSubHarm = exp(1i*Y*KSubHarmY1D)*diag(Coefs)*exp(1i*KSubHarmX1D*X);

% Sum and return
Screens = (ScreenHighFreq + ScreenSubHarm);
Phase1 = real(Screens);
Phase2 = imag(Screens);

return % pwdPhaseScreenMvk.m